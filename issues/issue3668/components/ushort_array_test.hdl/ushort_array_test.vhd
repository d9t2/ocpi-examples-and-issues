-- Copyright (c) 2023 Dominic Adam Walters
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
-- SOFTWARE.

library ieee;
use ieee.numeric_std.all;

-- ocpi.core
library ocpi;
use ocpi.types.all;

architecture rtl of worker is

  function fill_array(length: natural)
  return ushort_array_t is
    variable rv : ushort_array_t(0 to length - 1);
  begin
    for i in 0 to length - 1 loop
      rv(i) := to_unsigned(i + 1, rv(i)'length);
    end loop;
    return rv;
  end function fill_array;

begin

  ctl_out.finished <= '1';

  props_out.array_of_ushort_1 <= fill_array(props_out.array_of_ushort_1'length);
  props_out.array_of_ushort_2 <= fill_array(props_out.array_of_ushort_2'length);
  props_out.array_of_ushort_3 <= fill_array(props_out.array_of_ushort_3'length);
  props_out.array_of_ushort_4 <= fill_array(props_out.array_of_ushort_4'length);
  props_out.array_of_ushort_5 <= fill_array(props_out.array_of_ushort_5'length);
  props_out.array_of_ushort_6 <= fill_array(props_out.array_of_ushort_6'length);
  props_out.array_of_ushort_7 <= fill_array(props_out.array_of_ushort_7'length);
  props_out.array_of_ushort_8 <= fill_array(props_out.array_of_ushort_8'length);

end architecture rtl;