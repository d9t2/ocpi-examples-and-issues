# Description

This project is a
[MCVE](https://stackoverflow.com/help/minimal-reproducible-example) for
[issue #1382](https://gitlab.com/opencpi/opencpi/-/issues/1382).

This bug was fixed in [!520](https://gitlab.com/opencpi/opencpi/-/merge_requests/520).

Functions on the `array` types defined in the VHDL package `ocpi.types` don't
work on slices of these arrays.

## Development Environment

I developed this on CentOS 7 using [commit 380f6c63](https://gitlab.com/opencpi/opencpi/-/commit/380f6c63cfeed56cbe5881a4543f8e221a461753).

## Copyright

MIT License

Copyright (c) 2020 Dominic Adam Walters

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
