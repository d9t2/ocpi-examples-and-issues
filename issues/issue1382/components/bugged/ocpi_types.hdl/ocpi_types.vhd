-- Copyright (c) 2020 Dominic Adam Walters
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
-- SOFTWARE.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- ocpi.core
library ocpi;

architecture rtl of worker is

  constant C_ELEMENTS : natural := to_integer(elements);
  constant C_OFFSET   : natural := to_integer(offset);

  signal slv_bools_not_downto_0     : std_logic_vector(C_OFFSET + C_ELEMENTS - 1 downto
                                                       C_OFFSET);
  signal slv_chars_not_downto_0     : std_logic_vector(C_OFFSET + 8 * C_ELEMENTS - 1 downto
                                                       C_OFFSET);
  signal slv_shorts_not_downto_0    : std_logic_vector(C_OFFSET + 16 * C_ELEMENTS - 1 downto
                                                       C_OFFSET);
  signal slv_longs_not_downto_0     : std_logic_vector(C_OFFSET + 32 * C_ELEMENTS - 1 downto
                                                       C_OFFSET);
  signal slv_longlongs_not_downto_0 : std_logic_vector(C_OFFSET + 64 * C_ELEMENTS - 1 downto
                                                       C_OFFSET);
  signal slv_floats_not_downto_0    : std_logic_vector(C_OFFSET + 32 * C_ELEMENTS - 1 downto
                                                       C_OFFSET);
  signal slv_doubles_not_downto_0   : std_logic_vector(C_OFFSET + 64 * C_ELEMENTS - 1 downto
                                                       C_OFFSET);
  signal slv_strings_not_downto_0   : std_logic_vector(C_OFFSET + 8 * C_ELEMENTS - 1 downto
                                                       C_OFFSET);

  signal bool_array_not_from_0      : ocpi.types.    bool_array_t(C_OFFSET to
                                                                  C_OFFSET + C_ELEMENTS - 1);
  signal char_array_not_from_0      : ocpi.types.    char_array_t(C_OFFSET to
                                                                  C_OFFSET + C_ELEMENTS - 1);
  signal short_array_not_from_0     : ocpi.types.   short_array_t(C_OFFSET to
                                                                  C_OFFSET + C_ELEMENTS - 1);
  signal long_array_not_from_0      : ocpi.types.    long_array_t(C_OFFSET to
                                                                  C_OFFSET + C_ELEMENTS - 1);
  signal longlong_array_not_from_0  : ocpi.types.longlong_array_t(C_OFFSET to
                                                                  C_OFFSET + C_ELEMENTS - 1);

  signal uchar_array_not_from_0     : ocpi.types.    uchar_array_t(C_OFFSET to
                                                                   C_OFFSET + C_ELEMENTS - 1);
  signal ushort_array_not_from_0    : ocpi.types.   ushort_array_t(C_OFFSET to
                                                                   C_OFFSET + C_ELEMENTS - 1);
  signal ulong_array_not_from_0     : ocpi.types.    ulong_array_t(C_OFFSET to
                                                                   C_OFFSET + C_ELEMENTS - 1);
  signal ulonglong_array_not_from_0 : ocpi.types.ulonglong_array_t(C_OFFSET to
                                                                   C_OFFSET + C_ELEMENTS - 1);

  signal float_array_not_from_0     : ocpi.types.   float_array_t(C_OFFSET to
                                                                  C_OFFSET + C_ELEMENTS - 1);
  signal double_array_not_from_0    : ocpi.types.  double_array_t(C_OFFSET to
                                                                  C_OFFSET + C_ELEMENTS - 1);

  signal string_not_from_0          : ocpi.types.        string_t(C_OFFSET to
                                                                  C_OFFSET + C_ELEMENTS - 1);

  -- ###################
  -- # Fixed Functions #
  -- ###################
  function slv(a : bool_array_t) -- line 50 types_body.vhd
  return std_logic_vector is
    variable v    : std_logic_vector(a'length - 1 downto 0);
    variable i_v  : natural;
  begin
    for i_a in a'left to a'right loop
      i_v     := a'length - (i_a - a'left) - 1;
      v(i_v)  := a(i_a);
    end loop;
    return v;
  end function slv;

  function to_bool_array(a : std_logic_vector) -- line 62 types_body.vhd
  return bool_array_t is
    variable v    : bool_array_t(0 to a'length - 1);
    variable i_a  : natural;
  begin
    for i_v in 0 to v'right loop
      i_a     := v'length - i_v - 1;
      v(i_v)  := a(i_a + a'right);
    end loop;
    return v;
  end function to_bool_array;

  function slv(a : char_array_t) -- line 184 types_body.vhd
  return std_logic_vector is
    variable v    : std_logic_vector((8 * a'length) - 1 downto 0);
    variable i_v  : natural;
  begin
    for i_a in a'left to a'right loop
      i_v                                 := a'length - (i_a - a'left) - 1;
      v(8 * (i_v + 1) - 1 downto 8 * i_v) := from_char(a(i_a));
    end loop;
    return v;
  end function slv;

  function to_char_array(a : std_logic_vector) -- line 196 types_body.vhd
  return char_array_t is
    variable v    : char_array_t(0 to a'length / 8 - 1);
    variable i_a  : natural;
  begin
    for i_v in 0 to v'right loop
      i_a     := v'length - i_v - 1;
      v(i_v)  := char_t(a(8 * (i_a + 1) + a'right - 1 downto 8 * i_a + a'right));
    end loop;
    return v;
  end function to_char_array;

  function slv(a : short_array_t) -- line 214 types_body.vhd
  return std_logic_vector is
    variable v    : std_logic_vector((16 * a'length) - 1 downto 0);
    variable i_v  : natural;
  begin
    for i_a in a'left to a'right loop
      i_v                                   := a'length - (i_a - a'left) - 1;
      v(16 * (i_v + 1) - 1 downto 16 * i_v) := from_short(a(i_a));
    end loop;
    return v;
  end function slv;

  function to_short_array(a : std_logic_vector) -- line 226 types_body.vhd
  return short_array_t is
    variable v    : short_array_t(0 to a'length / 16 - 1);
    variable i_a  : natural;
  begin
    for i_v in 0 to v'right loop
      i_a     := v'length - i_v - 1;
      v(i_v)  := short_t(a(16 * (i_a + 1) + a'right - 1 downto 16 * i_a + a'right));
    end loop;
    return v;
  end function to_short_array;

  function slv(a : long_array_t) -- line 244 types_body.vhd
  return std_logic_vector is
    variable v    : std_logic_vector((32 * a'length) - 1 downto 0);
    variable i_v  : natural;
  begin
    for i_a in a'left to a'right loop
      i_v                                   := a'length - (i_a - a'left) - 1;
      v(32 * (i_v + 1) - 1 downto 32 * i_v) := from_long(a(i_a));
    end loop;
    return v;
  end function slv;

  function to_long_array(a : std_logic_vector) -- line 256 types_body.vhd
  return long_array_t is
    variable v    : long_array_t(0 to a'length / 32 - 1);
    variable i_a  : natural;
  begin
    for i_v in 0 to v'right loop
      i_a     := v'length - i_v - 1;
      v(i_v)  := long_t(a(32 * (i_a + 1) + a'right - 1 downto 32 * i_a + a'right));
    end loop;
    return v;
  end function to_long_array;

  function slv(a : longlong_array_t) -- line 380 types_body.vhd
  return std_logic_vector is
    variable v    : std_logic_vector((64 * a'length) - 1 downto 0);
    variable i_v  : natural;
  begin
    for i_a in a'left to a'right loop
      i_v                                   := a'length - (i_a - a'left) - 1;
      v(64 * (i_v + 1) - 1 downto 64 * i_v) := from_longlong(a(i_a));
    end loop;
    return v;
  end function slv;

  function to_longlong_array(a : std_logic_vector) -- line 392 types_body.vhd
  return longlong_array_t is
    variable v    : longlong_array_t(0 to a'length / 64 - 1);
    variable i_a  : natural;
  begin
    for i_v in 0 to v'right loop
      i_a     := v'length - i_v - 1;
      v(i_v)  := longlong_t(a(64 * (i_a + 1) + a'right - 1 downto 64 * i_a + a'right));
    end loop;
    return v;
  end function to_longlong_array;

  function slv(a : uchar_array_t) -- line 278 types_body.vhd
  return std_logic_vector is
    variable v    : std_logic_vector((8 * a'length) - 1 downto 0);
    variable i_v  : natural;
  begin
    for i_a in a'left to a'right loop
      i_v                                 := a'length - (i_a - a'left) - 1;
      v(8 * (i_v + 1) - 1 downto 8 * i_v) := from_uchar(a(i_a));
    end loop;
    return v;
  end function slv;

  function to_uchar_array(a : std_logic_vector) -- line 290 types_body.vhd
  return uchar_array_t is
    variable v    : uchar_array_t(0 to a'length / 8 - 1);
    variable i_a  : natural;
  begin
    for i_v in 0 to v'right loop
      i_a     := v'length - i_v - 1;
      v(i_v)  := uchar_t(a(8 * (i_a + 1) + a'right - 1 downto 8 * i_a + a'right));
    end loop;
    return v;
  end function to_uchar_array;

  function slv(a : ushort_array_t) -- line 308 types_body.vhd
  return std_logic_vector is
    variable v    : std_logic_vector((16 * a'length) - 1 downto 0);
    variable i_v  : natural;
  begin
    for i_a in a'left to a'right loop
      i_v                                   := a'length - (i_a - a'left) - 1;
      v(16 * (i_v + 1) - 1 downto 16 * i_v) := from_ushort(a(i_a));
    end loop;
    return v;
  end function slv;

  function to_ushort_array(a : std_logic_vector) -- line 320 types_body.vhd
  return ushort_array_t is
    variable v    : ushort_array_t(0 to a'length / 16 - 1);
    variable i_a  : natural;
  begin
    for i_v in 0 to v'right loop
      i_a     := v'length - i_v - 1;
      v(i_v)  := ushort_t(a(16 * (i_a + 1) + a'right - 1 downto 16 * i_a + a'right));
    end loop;
    return v;
  end function to_ushort_array;

  function slv(a : ulong_array_t) -- line 342 types_body.vhd
  return std_logic_vector is
    variable v    : std_logic_vector((32 * a'length) - 1 downto 0);
    variable i_v  : natural;
  begin
    for i_a in a'left to a'right loop
      i_v                                   := a'length - (i_a - a'left) - 1;
      v(32 * (i_v + 1) - 1 downto 32 * i_v) := from_ulong(a(i_a));
    end loop;
    return v;
  end function slv;

  function to_ulong_array(a : std_logic_vector) -- line 354 types_body.vhd
  return ulong_array_t is
    variable v    : ulong_array_t(0 to a'length / 32 - 1);
    variable i_a  : natural;
  begin
    for i_v in 0 to v'right loop
      i_a     := v'length - i_v - 1;
      v(i_v)  := ulong_t(a(32 * (i_a + 1) + a'right - 1 downto 32 * i_a + a'right));
    end loop;
    return v;
  end function to_ulong_array;

  function slv(a : ulonglong_array_t) -- line 418 types_body.vhd
  return std_logic_vector is
    variable v    : std_logic_vector((64 * a'length) - 1 downto 0);
    variable i_v  : natural;
  begin
    for i_a in a'left to a'right loop
      i_v                                   := a'length - (i_a - a'left) - 1;
      v(64 * (i_v + 1) - 1 downto 64 * i_v) := from_ulonglong(a(i_a));
    end loop;
    return v;
  end function slv;

  function to_ulonglong_array(a : std_logic_vector) -- line 430 types_body.vhd
  return ulonglong_array_t is
    variable v    : ulonglong_array_t(0 to a'length / 64 - 1);
    variable i_a  : natural;
  begin
    for i_v in 0 to v'right loop
      i_a     := v'length - i_v - 1;
      v(i_v)  := ulonglong_t(a(64 * (i_a + 1) + a'right - 1 downto 64 * i_a + a'right));
    end loop;
    return v;
  end function to_ulonglong_array;

  function slv(a : float_array_t) -- line 621 types_body.vhd
  return std_logic_vector is
    variable v    : std_logic_vector((32 * a'length) - 1 downto 0);
    variable i_v  : natural;
  begin
    for i_a in a'left to a'right loop
      i_v                                   := a'length - (i_a - a'left) - 1;
      v(32 * (i_v + 1) - 1 downto 32 * i_v) := from_float(a(i_a));
    end loop;
    return v;
  end function slv;

  function to_float_array(a : std_logic_vector) -- line 633 types_body.vhd
  return float_array_t is
    variable v    : float_array_t(0 to a'length / 32 - 1);
    variable i_a  : natural;
  begin
    for i_v in 0 to v'right loop
      i_a     := v'length - i_v - 1;
      v(i_v)  := float_t(a(32 * (i_a + 1) + a'right - 1 downto 32 * i_a + a'right));
    end loop;
    return v;
  end function to_float_array;

  function slv(a : double_array_t) -- line 655 types_body.vhd
  return std_logic_vector is
    variable v    : std_logic_vector((64 * a'length) - 1 downto 0);
    variable i_v  : natural;
  begin
    for i_a in a'left to a'right loop
      i_v                                   := a'length - (i_a - a'left) - 1;
      v(64 * (i_v + 1) - 1 downto 64 * i_v) := from_double(a(i_a));
    end loop;
    return v;
  end function slv;

  function to_double_array(a : std_logic_vector) -- line 667 types_body.vhd
  return double_array_t is
    variable v    : double_array_t(0 to a'length / 64 - 1);
    variable i_a  : natural;
  begin
    for i_v in 0 to v'right loop
      i_a     := v'length - i_v - 1;
      v(i_v)  := double_t(a(64 * (i_a + 1) + a'right - 1 downto 64 * i_a + a'right));
    end loop;
    return v;
  end function to_double_array;

  function to_string(v : std_logic_vector) -- line 504 types_body.vhd
  return string_t is
    variable s    : string_t(0 to v'length / 8 - 1);
    variable i_v  : natural;
  begin
    for i_s in 0 to s'right loop
      i_v     := s'length - i_s - 1;
      s(i_s)  := char_t(v(8 * (i_v + 1) + v'right - 1 downto 8 * i_v + v'right));
    end loop;
    return s;
  end function to_string;

  function from_string(s : string_t) -- line 550 types_body.vhd
  return std_logic_vector is
    variable v    : std_logic_vector((8 * s'length) - 1 downto 0);
    variable i_v  : natural;
  begin
    for i_s in s'left to s'right loop
      i_v                                 := s'length - (i_s - s'left) - 1;
      v(8 * (i_v + 1) - 1 downto 8 * i_v) := from_char(s(i_s));
    end loop;
    return v;
  end function from_string;

  function slv(a : string_t) -- line 572 types_body.vhd
  return std_logic_vector is
    variable v    : std_logic_vector((8 * a'length) - 1 downto 0);
    variable i_v  : natural;
  begin
    for i_a in a'left to a'right loop
      i_v                                 := a'length - (i_a - a'left) - 1;
      v(8 * (i_v + 1) - 1 downto 8 * i_v) := from_char(a(i_a));
    end loop;
    return v;
  end function slv;

  -- ######################
  -- # Non Bugged Signals #
  -- ######################
  signal slv_from_bool_array      : std_logic_vector(     C_ELEMENTS - 1 downto 0);
  signal slv_from_char_array      : std_logic_vector( 8 * C_ELEMENTS - 1 downto 0);
  signal slv_from_short_array     : std_logic_vector(16 * C_ELEMENTS - 1 downto 0);
  signal slv_from_long_array      : std_logic_vector(32 * C_ELEMENTS - 1 downto 0);
  signal slv_from_longlong_array  : std_logic_vector(64 * C_ELEMENTS - 1 downto 0);
  signal slv_from_uchar_array     : std_logic_vector( 8 * C_ELEMENTS - 1 downto 0);
  signal slv_from_ushort_array    : std_logic_vector(16 * C_ELEMENTS - 1 downto 0);
  signal slv_from_ulong_array     : std_logic_vector(32 * C_ELEMENTS - 1 downto 0);
  signal slv_from_ulonglong_array : std_logic_vector(64 * C_ELEMENTS - 1 downto 0);
  signal slv_from_float_array     : std_logic_vector(32 * C_ELEMENTS - 1 downto 0);
  signal slv_from_double_array    : std_logic_vector(64 * C_ELEMENTS - 1 downto 0);
  signal slv_from_string          : std_logic_vector( 8 * C_ELEMENTS - 1 downto 0);

  signal bool_array_from_slv      : ocpi.types.     bool_array_t(0 to C_ELEMENTS - 1);
  signal char_array_from_slv      : ocpi.types.     char_array_t(0 to C_ELEMENTS - 1);
  signal short_array_from_slv     : ocpi.types.    short_array_t(0 to C_ELEMENTS - 1);
  signal long_array_from_slv      : ocpi.types.     long_array_t(0 to C_ELEMENTS - 1);
  signal longlong_array_from_slv  : ocpi.types. longlong_array_t(0 to C_ELEMENTS - 1);
  signal uchar_array_from_slv     : ocpi.types.    uchar_array_t(0 to C_ELEMENTS - 1);
  signal ushort_array_from_slv    : ocpi.types.   ushort_array_t(0 to C_ELEMENTS - 1);
  signal ulong_array_from_slv     : ocpi.types.    ulong_array_t(0 to C_ELEMENTS - 1);
  signal ulonglong_array_from_slv : ocpi.types.ulonglong_array_t(0 to C_ELEMENTS - 1);
  signal float_array_from_slv     : ocpi.types.    float_array_t(0 to C_ELEMENTS - 1);
  signal double_array_from_slv    : ocpi.types.   double_array_t(0 to C_ELEMENTS - 1);
  signal string_from_slv          : ocpi.types.         string_t(0 to C_ELEMENTS - 1);

  -- ##################
  -- # Bugged Signals #
  -- ##################
  signal bugged_slv_from_bool_array       : std_logic_vector(     C_ELEMENTS - 1 downto 0);
  signal bugged_slv_from_char_array       : std_logic_vector( 8 * C_ELEMENTS - 1 downto 0);
  signal bugged_slv_from_short_array      : std_logic_vector(16 * C_ELEMENTS - 1 downto 0);
  signal bugged_slv_from_long_array       : std_logic_vector(32 * C_ELEMENTS - 1 downto 0);
  signal bugged_slv_from_longlong_array   : std_logic_vector(64 * C_ELEMENTS - 1 downto 0);
  signal bugged_slv_from_uchar_array      : std_logic_vector( 8 * C_ELEMENTS - 1 downto 0);
  signal bugged_slv_from_ushort_array     : std_logic_vector(16 * C_ELEMENTS - 1 downto 0);
  signal bugged_slv_from_ulong_array      : std_logic_vector(32 * C_ELEMENTS - 1 downto 0);
  signal bugged_slv_from_ulonglong_array  : std_logic_vector(64 * C_ELEMENTS - 1 downto 0);
  signal bugged_slv_from_float_array      : std_logic_vector(32 * C_ELEMENTS - 1 downto 0);
  signal bugged_slv_from_double_array     : std_logic_vector(64 * C_ELEMENTS - 1 downto 0);
  signal bugged_slv_from_string           : std_logic_vector( 8 * C_ELEMENTS - 1 downto 0);

  signal bugged_bool_array_from_slv       : ocpi.types.     bool_array_t(0 to C_ELEMENTS - 1);
  signal bugged_char_array_from_slv       : ocpi.types.     char_array_t(0 to C_ELEMENTS - 1);
  signal bugged_short_array_from_slv      : ocpi.types.    short_array_t(0 to C_ELEMENTS - 1);
  signal bugged_long_array_from_slv       : ocpi.types.     long_array_t(0 to C_ELEMENTS - 1);
  signal bugged_longlong_array_from_slv   : ocpi.types. longlong_array_t(0 to C_ELEMENTS - 1);
  signal bugged_uchar_array_from_slv      : ocpi.types.    uchar_array_t(0 to C_ELEMENTS - 1);
  signal bugged_ushort_array_from_slv     : ocpi.types.   ushort_array_t(0 to C_ELEMENTS - 1);
  signal bugged_ulong_array_from_slv      : ocpi.types.    ulong_array_t(0 to C_ELEMENTS - 1);
  signal bugged_ulonglong_array_from_slv  : ocpi.types.ulonglong_array_t(0 to C_ELEMENTS - 1);
  signal bugged_float_array_from_slv      : ocpi.types.    float_array_t(0 to C_ELEMENTS - 1);
  signal bugged_double_array_from_slv     : ocpi.types.   double_array_t(0 to C_ELEMENTS - 1);
  signal bugged_string_from_slv           : ocpi.types.         string_t(0 to C_ELEMENTS - 1);

begin

  -- ######################
  -- # Non Bugged Signals #
  -- ######################
  -- bool_array_t
  slv_from_bool_array       <= slv(bool_array_not_from_0);
  bool_array_from_slv       <= to_bool_array(slv_bools_not_downto_0);
  -- char_array_t
  slv_from_char_array       <= slv(char_array_not_from_0);
  char_array_from_slv       <= to_char_array(slv_chars_not_downto_0);
  -- short_array_t
  slv_from_short_array      <= slv(short_array_not_from_0);
  short_array_from_slv      <= to_short_array(slv_shorts_not_downto_0);
  -- long_array_t
  slv_from_long_array       <= slv(long_array_not_from_0);
  long_array_from_slv       <= to_long_array(slv_longs_not_downto_0);
  -- longlong_array_t
  slv_from_longlong_array   <= slv(longlong_array_not_from_0);
  longlong_array_from_slv   <= to_longlong_array(slv_longlongs_not_downto_0);
  -- uchar_array_t
  slv_from_uchar_array      <= slv(uchar_array_not_from_0);
  uchar_array_from_slv      <= to_uchar_array(slv_chars_not_downto_0);
  -- ushort_array_t
  slv_from_ushort_array     <= slv(ushort_array_not_from_0);
  ushort_array_from_slv     <= to_ushort_array(slv_shorts_not_downto_0);
  -- ulong_array_t
  slv_from_ulong_array      <= slv(ulong_array_not_from_0);
  ulong_array_from_slv      <= to_ulong_array(slv_longs_not_downto_0);
  -- ulonglong_array_t
  slv_from_ulonglong_array  <= slv(ulonglong_array_not_from_0);
  ulonglong_array_from_slv  <= to_ulonglong_array(slv_longlongs_not_downto_0);
  -- float_array_t
  slv_from_float_array      <= slv(float_array_not_from_0);
  float_array_from_slv      <= to_float_array(slv_floats_not_downto_0);
  -- double_array_t
  slv_from_double_array     <= slv(double_array_not_from_0);
  double_array_from_slv     <= to_double_array(slv_doubles_not_downto_0);
  -- string_t
  slv_from_string           <= from_string(string_not_from_0);
  string_from_slv           <= to_string(slv_strings_not_downto_0);

  -- ##################
  -- # Bugged Signals #
  -- ##################
  -- bool_array_t
  bug_1_gen   : if bug =  1 generate
    bugged_slv_from_bool_array      <= ocpi.types.slv(bool_array_not_from_0);
  end generate bug_1_gen;
  bug_2_gen   : if bug =  2 generate
    bugged_bool_array_from_slv      <= ocpi.types.to_bool_array(slv_bools_not_downto_0);
  end generate bug_2_gen;
  -- char_array_t
  bug_3_gen   : if bug =  3 generate
    bugged_slv_from_char_array      <= ocpi.types.slv(char_array_not_from_0);
  end generate bug_3_gen;
  bug_4_gen   : if bug =  4 generate
    bugged_char_array_from_slv      <= ocpi.types.to_char_array(slv_chars_not_downto_0);
  end generate bug_4_gen;
  -- short_array_t
  bug_5_gen   : if bug =  5 generate
    bugged_slv_from_short_array     <= ocpi.types.slv(short_array_not_from_0);
  end generate bug_5_gen;
  bug_6_gen   : if bug =  6 generate
    bugged_short_array_from_slv     <= ocpi.types.to_short_array(slv_shorts_not_downto_0);
  end generate bug_6_gen;
  -- long_array_t
  bug_7_gen   : if bug =  7 generate
    bugged_slv_from_long_array      <= ocpi.types.slv(long_array_not_from_0);
  end generate bug_7_gen;
  bug_8_gen   : if bug =  8 generate
    bugged_long_array_from_slv      <= ocpi.types.to_long_array(slv_longs_not_downto_0);
  end generate bug_8_gen;
  -- longlong_array_t
  bug_9_gen   : if bug =  9 generate
    bugged_slv_from_longlong_array  <= ocpi.types.slv(longlong_array_not_from_0);
  end generate bug_9_gen;
  bug_10_gen  : if bug = 10 generate
    bugged_longlong_array_from_slv  <= ocpi.types.to_longlong_array(slv_longlongs_not_downto_0);
  end generate bug_10_gen;
  -- uchar_array_t
  bug_11_gen  : if bug = 11 generate
    bugged_slv_from_uchar_array     <= ocpi.types.slv(uchar_array_not_from_0);
  end generate bug_11_gen;
  bug_12_gen  : if bug = 12 generate
    bugged_uchar_array_from_slv     <= ocpi.types.to_uchar_array(slv_chars_not_downto_0);
  end generate bug_12_gen;
  -- ushort_array_t
  bug_13_gen  : if bug = 13 generate
    bugged_slv_from_ushort_array    <= ocpi.types.slv(ushort_array_not_from_0);
  end generate bug_13_gen;
  bug_14_gen  : if bug = 14 generate
    bugged_ushort_array_from_slv    <= ocpi.types.to_ushort_array(slv_shorts_not_downto_0);
  end generate bug_14_gen;
  -- ulong_array_t
  bug_15_gen  : if bug = 15 generate
    bugged_slv_from_ulong_array     <= ocpi.types.slv(ulong_array_not_from_0);
  end generate bug_15_gen;
  bug_16_gen  : if bug = 16 generate
    bugged_ulong_array_from_slv     <= ocpi.types.to_ulong_array(slv_longs_not_downto_0);
  end generate bug_16_gen;
  -- ulonglong_array_t
  bug_17_gen  : if bug = 17 generate
    bugged_slv_from_ulonglong_array <= ocpi.types.slv(ulonglong_array_not_from_0);
  end generate bug_17_gen;
  bug_18_gen  : if bug = 18 generate
    bugged_ulonglong_array_from_slv <= ocpi.types.to_ulonglong_array(slv_longlongs_not_downto_0);
  end generate bug_18_gen;
  -- float_array_t
  bug_19_gen  : if bug = 19 generate
    bugged_slv_from_float_array     <= ocpi.types.slv(float_array_not_from_0);
  end generate bug_19_gen;
  bug_20_gen  : if bug = 20 generate
    bugged_float_array_from_slv     <= ocpi.types.to_float_array(slv_floats_not_downto_0);
  end generate bug_20_gen;
  -- double_array_t
  bug_21_gen  : if bug = 21 generate
    bugged_slv_from_double_array    <= ocpi.types.slv(double_array_not_from_0);
  end generate bug_21_gen;
  bug_22_gen  : if bug = 22 generate
    bugged_double_array_from_slv    <= ocpi.types.to_double_array(slv_doubles_not_downto_0);
  end generate bug_22_gen;
  -- string_t
  bug_23_gen  : if bug = 23 generate
    bugged_slv_from_string          <= ocpi.types.from_string(string_not_from_0);
  end generate bug_23_gen;
  bug_24_gen  : if bug = 24 generate
    bugged_string_from_slv          <= ocpi.types.to_string(slv_strings_not_downto_0);
  end generate bug_24_gen;

end architecture rtl;
