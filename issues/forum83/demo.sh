#!/usr/bin/env bash
set -e

# Copyright (c) 2024 Dominic Adam Walters
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# Location of this script
script_directory=$(cd "$(dirname "$0")" && pwd)

# Location to download to
cache_directory=/tmp/$(basename "$script_directory")

# Branch to download
branch="release-2.4.7"

# Import
source "$script_directory"/../../scripts/bash-utils.sh
source "$cache_directory"/opencpi/cdk/opencpi-setup.sh -r

# Clone and build `opencpi`
rm -rf "$cache_directory"
mkdir -p "$cache_directory"
git clone https://gitlab.com/opencpi/opencpi.git \
    --branch "$branch" \
    --single-branch \
    --depth 1 \
    "$cache_directory"/opencpi
cp -rf "$script_directory"/../../platforms/arch "$cache_directory"/opencpi/projects/core/rcc/platforms/.
cd "$cache_directory"/opencpi
for patch in $(find "$script_directory"/patches/opencpi -name "*.$branch.patch"); do
    git apply "$patch"
done
./scripts/install-opencpi.sh --minimal
source ./cdk/opencpi-setup.sh -r
ocpi_check_commands || exit $?
ocpi_build "HDL Primitives: ocpi.core" "$OCPI_ROOT_DIR"/projects/core/hdl/primitives --hdl-platform xsim
ocpi_build "HDL Platform: xsim" "$OCPI_ROOT_DIR"/projects/core/hdl/platforms/xsim --hdl-platform xsim --workers-as-needed

# Clone and build `ocpi.comp.sdr`
git clone https://gitlab.com/opencpi/comp/ocpi.comp.sdr.git \
    --branch "$branch" \
    --single-branch \
    --depth 1 \
    "$cache_directory"/opencpi/projects/ocpi.comp.sdr
cd "$cache_directory"/opencpi/projects/ocpi.comp.sdr
for patch in $(find "$script_directory"/patches/ocpi.comp.sdr -name "*.$branch.patch"); do
    git apply "$patch"
done
ocpi_register "$(pwd)"
ocpi_build "HDL Primitives: ocpi.comp.sdr" hdl/primitives --hdl-platform xsim
ocpi_build "HDL Worker: ocpi.comp.sdr.dsp.windower_l" components/dsp/windower_l.hdl --hdl-platform xsim --no-doc
ocpi_build "RCC Worker: ocpi.comp.sdr.dsp.windower_l_proxy" components/dsp/windower_l_proxy.rcc --no-doc

# Project setup
ocpi_register "$script_directory"
ocpi_build "HDL Assembly: windower_test_asm" "$script_directory"/hdl/assemblies/windower_test_asm --hdl-platform xsim --workers-as-needed
cd "$script_directory"/applications
set +e
OCPI_LOG_LEVEL=8 \
    OCPI_LIBRARY_PATH="$script_directory"/artifacts:"$cache_directory"/opencpi/projects/core/artifacts:"$cache_directory"/opencpi/projects/ocpi.comp.sdr/artifacts \
        ocpirun windower_test.xml -t 5
exit_code=$?
set -e
if [ "$exit_code" -ne 0 ]; then
  print_error "Bug is present"
  exit "$exit_code"
fi
print_success "Bug is not present"
