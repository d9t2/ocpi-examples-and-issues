#!/usr/bin/env bash
set -e

# Copyright (c) 2023 Dominic Adam Walters
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# Location of this script
script_directory=$(cd $(dirname $0) && pwd)

# Import
source $script_directory/../../scripts/bash-utils.sh

# Project setup
ocpi_check_commands || exit $?
ocpi_register $script_directory
ocpi_build "Primitives: ocpi.core" \
  $OCPI_ROOT_DIR/projects/core/hdl/primitives \
  --hdl-platform xsim
ocpi_build "Platform: ocpi.core.xsim" \
  $OCPI_ROOT_DIR/projects/core/hdl/platforms/xsim \
  --hdl-platform xsim --workers-as-needed
ocpi_build "Project" $script_directory \
  --hdl-platform xsim --workers-as-needed

# Run
section "Run Application"
set +e

OCPI_LIBRARY_PATH=$script_directory/artifacts \
  OCPI_LOG_LEVEL=8 \
  ocpirun $script_directory/applications/test.xml -t 1
exit_code=$?
if [ $exit_code -ne 0 ]; then
  print_error "Bug is present"
  exit $exit_code
fi
print_success "Bug is not present"

set -e
end_current_section
