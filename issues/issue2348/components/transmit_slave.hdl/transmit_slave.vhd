-- Copyright (c) 2021 Dominic Adam Walters
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to
-- deal in the Software without restriction, including without limitation the
-- rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
-- sell copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
-- IN THE SOFTWARE.

library ieee;
use ieee.std_logic_1164.all;

-- ocpi.core
library ocpi;
use ocpi.types.all;

architecture rtl of worker is

  signal rf_output_out_sig      : worker_rf_output_out_t;
  signal to_controller_out_sig  : worker_to_controller_out_t;
  signal to_switch_out_sig      : worker_to_switch_out_t;

begin

  ctl_out.finished <=
  rf_output_out_sig.eof
  and from_controller_in.eof
  and to_controller_out_sig.eof
  and from_switch_in.eof
  and to_switch_out_sig.eof;

  -- Controller to Switch
  from_controller_out.take      <= to_switch_in.ready;
  to_switch_out_sig.give        <= from_controller_in.ready;
  to_switch_out_sig.data        <= from_controller_in.data;
  to_switch_out_sig.byte_enable <= from_controller_in.byte_enable;
  to_switch_out_sig.opcode      <= from_controller_in.opcode;
  to_switch_out_sig.som         <= from_controller_in.som;
  to_switch_out_sig.eom         <= from_controller_in.eom;
  to_switch_out_sig.valid       <= from_controller_in.valid;
  to_switch_out_sig.eof         <= from_controller_in.eof;

  to_switch_out <= to_switch_out_sig;

  -- Switch to RF or Controller

  from_switch_proc : process(props_in, rf_output_in, to_controller_in, from_switch_in)
  begin
    if its(props_in.use_rf) then
      from_switch_out.take              <= rf_output_in.ready;
      rf_output_out_sig.give            <= from_switch_in.ready;
      rf_output_out_sig.data            <= from_switch_in.data;
      rf_output_out_sig.byte_enable     <= from_switch_in.byte_enable;
      rf_output_out_sig.opcode          <= from_switch_in.opcode;
      rf_output_out_sig.som             <= from_switch_in.som;
      rf_output_out_sig.eom             <= from_switch_in.eom;
      rf_output_out_sig.valid           <= from_switch_in.valid;
      rf_output_out_sig.eof             <= from_switch_in.eof;
      to_controller_out_sig.give        <= '0';
      to_controller_out_sig.data        <= (others => '0');
      to_controller_out_sig.byte_enable <= (others => '0');
      to_controller_out_sig.opcode      <= (others => '0');
      to_controller_out_sig.som         <= '0';
      to_controller_out_sig.eom         <= '0';
      to_controller_out_sig.valid       <= '0';
      to_controller_out_sig.eof         <= '0';
    else
      from_switch_out.take              <= to_controller_in.ready;
      rf_output_out_sig.give            <= '0';
      rf_output_out_sig.data            <= (others => '0');
      rf_output_out_sig.byte_enable     <= (others => '0');
      rf_output_out_sig.opcode          <= (others => '0');
      rf_output_out_sig.som             <= '0';
      rf_output_out_sig.eom             <= '0';
      rf_output_out_sig.valid           <= '0';
      rf_output_out_sig.eof             <= '0';
      to_controller_out_sig.give        <= from_switch_in.ready;
      to_controller_out_sig.data        <= from_switch_in.data;
      to_controller_out_sig.byte_enable <= from_switch_in.byte_enable;
      to_controller_out_sig.opcode      <= from_switch_in.opcode;
      to_controller_out_sig.som         <= from_switch_in.som;
      to_controller_out_sig.eom         <= from_switch_in.eom;
      to_controller_out_sig.valid       <= from_switch_in.valid;
      to_controller_out_sig.eof         <= from_switch_in.eof;
    end if;
  end process from_switch_proc;

  rf_output_out     <= rf_output_out_sig;
  to_controller_out <= to_controller_out_sig;

end architecture rtl;
