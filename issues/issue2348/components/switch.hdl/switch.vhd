-- Copyright (c) 2021 Dominic Adam Walters
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to
-- deal in the Software without restriction, including without limitation the
-- rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
-- sell copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
-- IN THE SOFTWARE.

library ieee;
use ieee.std_logic_1164.all;

-- ocpi.core
library ocpi;
use ocpi.types.all;

architecture rtl of worker is

  signal output_out_sig   : worker_output_out_t;
  signal output0_out_sig  : worker_output0_out_t;
  signal output1_out_sig  : worker_output1_out_t;

begin

  input_proc : process(props_in, input_in, output0_in, output1_in)
  begin
    if its(props_in.use_channel_0) then
      input_out.take              <= output0_in.ready;
      output0_out_sig.give        <= input_in.ready;
      output0_out_sig.data        <= input_in.data;
      output0_out_sig.byte_enable <= input_in.byte_enable;
      output0_out_sig.opcode      <= input_in.opcode;
      output0_out_sig.som         <= input_in.som;
      output0_out_sig.eom         <= input_in.eom;
      output0_out_sig.valid       <= input_in.valid;
      output0_out_sig.eof         <= input_in.eof;
      output1_out_sig.give        <= '0';
      output1_out_sig.data        <= (others => '0');
      output1_out_sig.byte_enable <= (others => '0');
      output1_out_sig.opcode      <= (others => '0');
      output1_out_sig.som         <= '0';
      output1_out_sig.eom         <= '0';
      output1_out_sig.valid       <= '0';
      output1_out_sig.eof         <= '0';
    else
      input_out.take              <= output1_in.ready;
      output0_out_sig.give        <= '0';
      output0_out_sig.data        <= (others => '0');
      output0_out_sig.byte_enable <= (others => '0');
      output0_out_sig.opcode      <= (others => '0');
      output0_out_sig.som         <= '0';
      output0_out_sig.eom         <= '0';
      output0_out_sig.valid       <= '0';
      output0_out_sig.eof         <= '0';
      output1_out_sig.give        <= input_in.ready;
      output1_out_sig.data        <= input_in.data;
      output1_out_sig.byte_enable <= input_in.byte_enable;
      output1_out_sig.opcode      <= input_in.opcode;
      output1_out_sig.som         <= input_in.som;
      output1_out_sig.eom         <= input_in.eom;
      output1_out_sig.valid       <= input_in.valid;
      output1_out_sig.eof         <= input_in.eof;
    end if;
  end process input_proc;

  output0_out <= output0_out_sig;
  output1_out <= output1_out_sig;

  output_proc : process(props_in, output_in, input0_in, input1_in)
  begin
    if its(props_in.use_channel_0) then
      input0_out.take             <= output_in.ready;
      input1_out.take             <= '0';
      output_out_sig.give         <= input0_in.ready;
      output_out_sig.data         <= input0_in.data;
      output_out_sig.byte_enable  <= input0_in.byte_enable;
      output_out_sig.opcode       <= input0_in.opcode;
      output_out_sig.som          <= input0_in.som;
      output_out_sig.eom          <= input0_in.eom;
      output_out_sig.valid        <= input0_in.valid;
      output_out_sig.eof          <= input0_in.eof;
    else
      input0_out.take             <= '0';
      input1_out.take             <= output_in.ready;
      output_out_sig.give         <= input1_in.ready;
      output_out_sig.data         <= input1_in.data;
      output_out_sig.byte_enable  <= input1_in.byte_enable;
      output_out_sig.opcode       <= input1_in.opcode;
      output_out_sig.som          <= input1_in.som;
      output_out_sig.eom          <= input1_in.eom;
      output_out_sig.valid        <= input1_in.valid;
      output_out_sig.eof          <= input1_in.eof;
    end if;
  end process output_proc;

  output_out <= output_out_sig;

end architecture rtl;
