-- Copyright (c) 2021 Dominic Adam Walters
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to
-- deal in the Software without restriction, including without limitation the
-- rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
-- sell copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
-- IN THE SOFTWARE.

library ieee;
use ieee.std_logic_1164.all;

-- ocpi.core
library ocpi;
use ocpi.types.all;

architecture rtl of worker is

  signal to_controller_out_sig : worker_to_controller_out_t;
  signal to_switch_out_sig     : worker_to_switch_out_t;

begin

  ctl_out.finished <=
    rf_input_in.eof
    and from_controller_in.eof
    and to_controller_out_sig.eof
    and from_switch_in.eof
    and to_switch_out_sig.eof;

  -- Controller from Switch
  from_switch_out.take              <= to_controller_in.ready;
  to_controller_out_sig.give        <= from_switch_in.ready;
  to_controller_out_sig.data        <= from_switch_in.data;
  to_controller_out_sig.byte_enable <= from_switch_in.byte_enable;
  to_controller_out_sig.opcode      <= from_switch_in.opcode;
  to_controller_out_sig.som         <= from_switch_in.som;
  to_controller_out_sig.eom         <= from_switch_in.eom;
  to_controller_out_sig.valid       <= from_switch_in.valid;
  to_controller_out_sig.eof         <= from_switch_in.eof;

  to_controller_out <= to_controller_out_sig;

  -- Switch from RF or Controller
  to_switch_proc : process(props_in, to_switch_in, rf_input_in, from_controller_in)
  begin
    if its(props_in.use_rf) then
      rf_input_out.take             <= to_switch_in.ready;
      from_controller_out.take      <= '0';
      to_switch_out_sig.give        <= rf_input_in.ready;
      to_switch_out_sig.data        <= rf_input_in.data;
      to_switch_out_sig.byte_enable <= rf_input_in.byte_enable;
      to_switch_out_sig.opcode      <= rf_input_in.opcode;
      to_switch_out_sig.som         <= rf_input_in.som;
      to_switch_out_sig.eom         <= rf_input_in.eom;
      to_switch_out_sig.valid       <= rf_input_in.valid;
      to_switch_out_sig.eof         <= rf_input_in.eof;
    else
      rf_input_out.take             <= '0';
      from_controller_out.take      <= to_switch_in.ready;
      to_switch_out_sig.give        <= from_controller_in.ready;
      to_switch_out_sig.data        <= from_controller_in.data;
      to_switch_out_sig.byte_enable <= from_controller_in.byte_enable;
      to_switch_out_sig.opcode      <= from_controller_in.opcode;
      to_switch_out_sig.som         <= from_controller_in.som;
      to_switch_out_sig.eom         <= from_controller_in.eom;
      to_switch_out_sig.valid       <= from_controller_in.valid;
      to_switch_out_sig.eof         <= from_controller_in.eof;
    end if;
  end process to_switch_proc;

  to_switch_out <= to_switch_out_sig;

end architecture rtl;
