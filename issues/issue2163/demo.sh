#!/usr/bin/env bash
set -e

# Copyright (c) 2023 Dominic Adam Walters
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# Location of this script
script_directory=$(cd $(dirname $0) && pwd)

# Import
source $script_directory/../../scripts/bash-utils.sh

# Project setup
ocpi_check_commands || exit $?
ocpi_register $script_directory

# Build and run `send.rcc`
ocpi_build "Send Worker" $script_directory/components/send.rcc --no-doc
platform=$(find $script_directory/components/send.rcc -name "target-*" \
  | sed "s/.*-\(.*\)/\1/g")
ocpi_build "Send Test" $script_directory/components/send.test
section "Run Test on Send Worker"
set +e

ocpidev run test -d $script_directory/components/send.test
grep --quiet "Segmentation fault" $script_directory/components/send.test/run/$platform/case00.00.send.rcc.log
if [ $? -eq 0 ]; then
  send_worker_failed=1
fi
## This clean is needed to force the second test run to use the other worker
ocpidev clean -d $script_directory

set -e
end_current_section

# Build and run `send2.rcc`
ocpi_build "Send2 Worker" $script_directory/components/send2.rcc --no-doc
ocpi_build "Send Test" $script_directory/components/send.test
section "Run Test on Send2 Worker"
set +e

ocpidev run test -d $script_directory/components/send.test
grep --quiet "Segmentation fault" $script_directory/components/send.test/run/$platform/case00.00.send2.rcc.log
if [ $? -eq 0 ]; then
  send2_worker_failed=1
fi

set -e
end_current_section

# Print what happened
if [ -n $send_worker_failed ]; then
  print_error "Bug is present: send.rcc segfaulted the test"
fi
if [ -n $send2_worker_failed ]; then
  print_error "Bug is present: send2.rcc segfaulted the test"
fi
if [ -n $send_worker_failed ] || [ -n $send2_worker_failed ]; then
  exit 1
fi
print_success "Bug is not present"
