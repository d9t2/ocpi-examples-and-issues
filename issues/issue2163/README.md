# Description

This project is a
[MCVE](https://stackoverflow.com/help/minimal-reproducible-example) for
[issue #2163](https://gitlab.com/opencpi/opencpi/-/issues/2163).

It is still present in `release-v2.4.6`.

Calling `send` in an `rccworker` twice causes a segfault.

This also occurs when `send`ing a `take`.

## Development Environment

I developed this on CentOS 7 on commit [22ea69f6](https://gitlab.com/opencpi/opencpi/-/commit/22ea69f671f58f48ce0b9f942e04c383f8546bc6).

## Copyright

MIT License

Copyright (c) 2021 Dominic Adam Walters

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
