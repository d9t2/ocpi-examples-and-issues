# Description

This project is a
[MCVE](https://stackoverflow.com/help/minimal-reproducible-example) for a
[bug noted on the OpenCPI forum](https://opencpi.dev/t/55), that has since been
officially reported in [issue #3623](https://gitlab.com/opencpi/opencpi/-/issues/3623).

This bug was fixed in [!1121](https://gitlab.com/opencpi/opencpi/-/merge_requests/1121).

When a worker has a `start` method that returns `RCC_FATAL`, the application
isn't immediately killed.

When the application gets to the next control operation (tested with `release`,
but probably also `stop`), the application will print a stack trace.

## Development Environment

I developed this on Ubuntu 22.04 using `develop` branch from around June 2023.

## Copyright

MIT License

Copyright (c) 2023 Dominic Adam Walters

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
