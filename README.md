# OpenCPI: Examples and Issue MCVEs

## Introduction

This repository is a collection of OpenCPI Projects that are either:

- Examples of some piece of functionality within OpenCPI, normally produced
  after a request from a colleague or somebody on the forum.
- [MCVEs](https://stackoverflow.com/help/minimal-reproducible-example) for an
  OpenCPI issue.

## Structure

There are three main folders:

- `examples`
- `issues`
- `tools`

### Examples

This directory contains projects that provide a worked example of some feature
of OpenCPI.

They are all standalone, and each feature their own `README.md` as well as a
`demo.sh` script that should setup, build, and run the example.

### Issues

This directory contains projects that demonstrate an issue that has been
recorded on the [OpenCPI gitlab](https://gitlab.com/opencpi/opencpi/-/issues).

They are all standalone, and each feature their own `README.md` as well as a
`demo.sh` script that should demonstrate the problem.

### Tools

This directory contains tools that provide some useful function relating to OpenCPI.

## Copyright

MIT License

Copyright (c) 2020 Dominic Adam Walters

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
