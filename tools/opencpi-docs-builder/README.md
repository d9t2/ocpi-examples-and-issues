# OpenCPI Documentation Builder

The `Makefile` in this folder generates an OCI Container that builds the OpenCPI documentation for
a specific release.

## Dependencies

- `docker` or `podman`
    - If you have both installed, `podman` will be used.

## Build

To build the OpenCPI documentation for `v2.4.6`:

```bash
make build
```

If you want to specify a different tag:

```bash
make build tag=v2.4.7
```

> :bulb: Invoking `make build` will create an image called `opencpi-docs-$tag`.
