#!/usr/bin/env bash

# Copyright (c) 2023 Dominic Adam Walters
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

##############
# Formatting #
##############
if [ -z $D9T2_ENABLE_COLOUR ]; then
  D9T2_ENABLE_COLOUR=1
fi
if [ -z $D9T2_COLOUR_BLUE ]; then
  D9T2_COLOUR_BLUE="\e[34m"
fi
if [ -z $D9T2_COLOUR_GREEN ]; then
  D9T2_COLOUR_GREEN="\e[32m"
fi
if [ -z $D9T2_COLOUR_RED ]; then
  D9T2_COLOUR_RED="\e[31m"
fi
if [ -z $D9T2_COLOUR_YELLOW ]; then
  D9T2_COLOUR_YELLOW="\e[33m"
fi
if [ -z $D9T2_COLOUR_END ]; then
  D9T2_COLOUR_END="\e[0m"
fi

# Output a string in the "good" colour.
#   Arguments:
#     $1: A string.
format_good() {
  if [ $D9T2_ENABLE_COLOUR -eq 1 ]; then
    printf "$D9T2_COLOUR_GREEN$1$D9T2_COLOUR_END"
  else
    printf $1
  fi
}

# Output a string in the "bad" colour.
#   Arguments:
#     $1: A string.
format_bad() {
  if [ $D9T2_ENABLE_COLOUR -eq 1 ]; then
    printf "$D9T2_COLOUR_RED$1$D9T2_COLOUR_END"
  else
    printf $1
  fi
}

# Output a string in the "warning" colour.
#   Arguments:
#     $1: A string.
format_warning() {
  if [ $D9T2_ENABLE_COLOUR -eq 1 ]; then
    printf "$D9T2_COLOUR_YELLOW$1$D9T2_COLOUR_END"
  else
    printf $1
  fi
}

# Output a string in the "info" colour.
#   Arguments:
#     $1: A string.
format_info() {
  if [ $D9T2_ENABLE_COLOUR -eq 1 ]; then
    printf "$D9T2_COLOUR_BLUE$1$D9T2_COLOUR_END"
  else
    printf $1
  fi
}

# Variable that gets set when a section has started and not ended.
# Value is the name of the section.
D9T2_CURRENT_SECTION=

# Start a section, and print its name.
#   Arguments:
#     $1: A string.
section() {
  echo -e $(format_info "# $1")
  D9T2_CURRENT_SECTION="$1"
}

# End a section, and print that it has ended.
end_current_section() {
  echo -e $(format_info "# End of \`$D9T2_CURRENT_SECTION\`")"\n"
  D9T2_CURRENT_SECTION=
}

# Print a string formatted as a success.
#   Arguments:
#     $1: A string.
print_success() {
  echo -e $(format_good "$1")
}

# Print a string formatted as an error.
#   Arguments:
#     $1: A string.
print_error() {
  echo -e $(format_bad "$1")
}

# Print a string formatted as a warning.
#   Arguments:
#     $1: A string.
print_warning() {
  echo -e $(format_warning "$1")
}

# Start a block of code that can be ctrl-c terminated.
# This prints a message declaring that termination is available, and disables
# errors so the script won't exit with one ctrl-c.
start_terminable_block() {
  print_warning "Use ctrl-c to terminate..."
  set +e
}

# End a block of code that can be ctrl-c terminated.
# This enables errors so the script will now exit with any failed command.
end_terminable_block() {
  echo ""
  set -e
}

##########
# Errors #
##########
# Echo to stderr.
#   Arguments:
#     $1: A string.
echoerr() {
  >&2 echo -e $(format_bad "$1\n")
}

###############
# Environment #
###############
# Check whether a string is available as a command.
#   Arguments:
#     $1: A command name.
#   Returns:
#       0: If $1 is an available command.
#     127: If $1 is not an available command.
check_command() {
  if ! command -v "$1" &> /dev/null; then
    echoerr "\`$1\` is not in \$PATH"
    return 127
  fi
}

# Check whether multiple strings are available as commands.
#   Arguments:
#     $@: A list of command names.
#   Returns:
#       0: If all words in $@ are available commands.
#     127: If any word in $@ is not an available command.
check_commands() {
  for command in $@; do
    check_command $command || return $?
    echo -e $(format_good "\`$command\` is available")
  done
}

###########
# OpenCPI #
###########
# Check whether the main OpenCPI commands are available.
#   Returns:
#       0: If all commands are available.
#     127: If any command is not available.
ocpi_check_commands() {
  section "OpenCPI Commands"
  check_commands "ocpidev" "ocpirun" || return $?
  end_current_section
}

# Register a project.
#   Arguments:
#     $1: A directory containing a project.
ocpi_register() {
  section "Register Project"
  ocpidev register project -d $1 || return $?
  end_current_section
}

# Build a directory.
#   Arguments:
#     $1: A name representing what is in the directory.
#     $2: A directory.
#     $3+: Any further arguments for `ocpidev`.
ocpi_build() {
  section "Build $1"
  ocpidev build -d $2 ${@:3} || return $?
  end_current_section
}
