#!/usr/bin/env bash
set -e

# Copyright (c) 2023 Dominic Adam Walters
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# Location of this script
script_directory=$(cd $(dirname $0) && pwd)

# Import
source $script_directory/../../scripts/bash-utils.sh

# Project setup
application_name=file_io_benchmark
application_directory=$script_directory/applications/$application_name
ocpi_check_commands || exit $?
ocpi_register $script_directory
ocpi_build "Project" $script_directory
section "Create \`input.dat\`"
if [ ! -f $application_directory/1MB.dat ]; then
  dd \
    if=/dev/urandom \
    of=$application_directory/1MB.dat \
    bs=1M count=1
fi
if [ ! -f $application_directory/1200MB.dat ]; then
  dd \
    if=/dev/urandom \
    of=$application_directory/1200MB.dat \
    bs=1M count=1200
fi
end_current_section

# Run benchmark
run_application() {
  local file_read=$1
  local file_write=$2
  local duration=$3
  local messagesize=$4
  local granularity=$5
  local repeat=$6
  local filename=$7
  pushd $application_directory >> /dev/null
  OCPI_LIBRARY_PATH=$script_directory/artifacts/:$OCPI_ROOT_DIR/projects/core/artifacts \
    ocpirun $application_name.xml \
      -d \
      -t $duration \
      -wfile_read=$file_read \
      -wfile_write=$file_write \
      -pfile_read=filename=$filename \
      -pfile_read=messagesize=$messagesize \
      -pfile_read=granularity=$granularity \
      -pfile_read=repeat=$repeat \
    |& grep "file_read.bytesRead" \
    | tail -n 1 \
    | tr -s " " \
    | cut -d" " -f 5 \
    | xargs echo "$file_read $file_write $duration $messagesize $granularity $repeat"
  popd >> /dev/null
}

benchmark_messagesize() {
  local file_read=$1
  local file_write=$2
  echo -e $(format_info "# Varying Message Size - $file_read / $file_write")
  sizes=$(python -c "print(' '.join([str(2**x) for x in range(14)]))")
  # Note: 2^14, 2^15, and 2^16 don't work
  for size in $sizes; do
    run_application $file_read $file_write 1 $size 1 false $application_directory/1200MB.dat
  done
}

benchmark_granularity() {
  local file_read=$1
  local file_write=$2
  echo -e $(format_info "# Varying Granularity - $file_read / $file_write")
  granularities=$(python -c "print(' '.join([str(2**x) for x in range(13)]))")
  for granularity in $granularities; do
    run_application $file_read $file_write 1 4096 $granularity false $application_directory/1200MB.dat
  done
}

benchmark_repeat() {
  local file_read=$1
  local file_write=$2
  echo -e $(format_info "# Varying Message Size with Repeat - $file_read / $file_write")
  sizes=$(python -c "print(' '.join([str(2**x) for x in range(14)]))")
  # Note: 2^14, 2^15, and 2^16 don't work
  for size in $sizes; do
    run_application $file_read $file_write 1 $size 1 true $application_directory/1MB.dat
  done
}

results=$script_directory/benchmark_results.txt

run_benchmark() {
  local file_read=$1
  local file_write=$2
  section "Run Benchmark - $file_read / $file_write"
  benchmark_messagesize $file_read $file_write | tee -a $results
  benchmark_granularity $file_read $file_write | tee -a $results
  benchmark_repeat $file_read $file_write | tee -a $results
  end_current_section
}


rm -f $results
touch $results

set +e

run_benchmark file_read file_write
run_benchmark file_read_cc file_write
run_benchmark file_read file_write_cc
run_benchmark file_read_cc file_write_cc

set -e
