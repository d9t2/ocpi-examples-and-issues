// Copyright (c) 2023 Dominic Adam Walters
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

#include "file_read_cc-worker.hh"

#include <fstream>

using namespace File_read_ccWorkerTypes;

struct header_t {
  uint32_t length;
  uint32_t opcode;
};
static constexpr size_t HEADER_LENGTH = sizeof(header_t);

class File_read_ccWorker : public File_read_ccWorkerBase {

  std::ifstream _file_stream;
  ushort _default_message_length;

  public: File_read_ccWorker()
    : _file_stream(),
      _default_message_length() {
  }

  OCPI::RCC::RCCResult start() {
    auto props = this->properties();
    auto port = this->out;
    // Open file if not already open
    //  - This is to make it safe to `start` again after a `stop`
    if (!this->_file_stream.is_open()) {
      this->_file_stream.open(props.fileName, std::ios::binary | std::ios::in);
      if (this->_file_stream.fail()) {
        return this->setError("file `%s` could not be opened", props.fileName);
      }
    }
    // Set default length and opcode
    if (props.granularity != 0) {
      props.messageSize -= props.messageSize % props.granularity;
    }
    this->_default_message_length = port.maxLength();
    if (props.messageSize != 0) {
      if (props.messageSize > port.maxLength()) {
        this->log(8,
          "message size (%u) larger than max buffer size (%zu), "
          "overriding buffer size",
			    props.messageSize, port.maxLength()
        );
      }
      this->_default_message_length = props.messageSize;
    }
    port.setDefaultLength(this->_default_message_length);
    port.setDefaultOpCode(props.opcode);
    return OCPI::RCC::RCC_OK;
  }

  OCPI::RCC::RCCResult run(bool /*timedout*/) {
    auto &props = this->m_properties;
    auto &port = this->out;
    // Parse messages in file if necessary
    bool zero_length_message = false;
    if (props.messagesInFile) {
      header_t header;
      this->_file_stream.read(reinterpret_cast<char*>(&header), HEADER_LENGTH);
      const size_t actual_header_size = this->_file_stream.gcount();
      const bool read_was_blank = actual_header_size == 0;
      const bool header_fully_read = actual_header_size == HEADER_LENGTH;
      if (!read_was_blank && (!header_fully_read || this->_file_stream.fail())) {
        props.badMessage = true;
        return this->setError(
          "can't read message header from file (%zu)",
          actual_header_size
        );
      }
      zero_length_message = header_fully_read && header.length == 0;
      if (header_fully_read) {
        if (header.length > port.maxLength()) {
          props.badMessage = true;
          return this->setError(
            "message header length (%zu) larger than buffer (%zu)",
            header.length, port.maxLength()
          );
        }
        port.setOpCode((OCPI::RCC::RCCOpCode)header.opcode);
        port.setLength(header.length);
      } else {
        port.setOpCode(props.opcode);
        port.setLength(0);
      }
    }
    // Pull data from file
    this->_file_stream.read(reinterpret_cast<char*>(port.data()), port.length());
    if (!this->_file_stream.eof() && this->_file_stream.fail()) {
      return this->setError("error reading file");
    }
    // Check output length was valid, and use it
    const size_t actual_length = this->_file_stream.gcount();
    this->log(8, "read %zu bytes", actual_length);
    if (actual_length != 0 && actual_length != port.length()) {
      if (props.messagesInFile || (!props.messagesInFile && !this->_file_stream.eof())) {
        props.badMessage = props.messagesInFile;
        return this->setError(
          "actual message size (%zu) different from %s (%zu)",
          actual_length,
          props.messagesInFile ? "header length" : "message size",
          port.length()
        );
      }
    }
    port.setLength(actual_length);
    props.bytesRead += actual_length;
    // Send message if there is one
    if (actual_length != 0 || zero_length_message) {
      props.messagesWritten += 1;
      return OCPI::RCC::RCC_ADVANCE;
    }
    // File has finished, start repeat if enabled
    if (props.repeat) {
      this->_file_stream.clear();
      this->_file_stream.seekg(0, this->_file_stream.beg);
      if (this->_file_stream.fail()) {
        return this->setError("error rewinding file");
      }
      port.setLength(this->_default_message_length);
      this->log(8, "repeating input file");
      return OCPI::RCC::RCC_OK;
    }
    // File has finished, no repeat, so close file and `eof` if required
    if (props.suppressEOF) {
      this->log(8, "suppressing EoF");
      return this->close_file_with_result(OCPI::RCC::RCC_DONE);
    }
    this->log(8, "sending EoF");
    port.setEOF();
    return this->close_file_with_result(OCPI::RCC::RCC_ADVANCE_DONE);
  }

  OCPI::RCC::RCCResult release() {
    return this->close_file_with_result(OCPI::RCC::RCC_OK);
  }

  OCPI::RCC::RCCResult close_file_with_result(OCPI::RCC::RCCResult result) {
    if (this->_file_stream.is_open()) {
      try {
        this->_file_stream.close();
      } catch (std::exception &e) {
        return this->setError("Exception thrown when closing file");
      }
    }
    return result;
  }

};

FILE_READ_CC_START_INFO
FILE_READ_CC_END_INFO
