// Copyright (c) 2023 Dominic Adam Walters
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

#include "file_write_cc-worker.hh"

#include <fstream>

using namespace File_write_ccWorkerTypes;

struct header_t {
  uint32_t length;
  uint32_t opcode;
};
static constexpr size_t HEADER_LENGTH = sizeof(header_t);

class File_write_ccWorker : public File_write_ccWorkerBase {

  std::ofstream _file_stream;

  public: File_write_ccWorker()
    : _file_stream() {
  }

  OCPI::RCC::RCCResult start() {
    auto props = this->properties();
    auto port = this->in;
    // Open the file if not already open
    //  - This might require creation or truncation
    if (!this->_file_stream.is_open()) {
      this->_file_stream.open(props.fileName, std::ios::binary | std::ios::out);
      if (this->_file_stream.fail()) {
        return this->setError("file `%s` could not be opened", props.fileName);
      }
    }
    return OCPI::RCC::RCC_OK;
  }

  OCPI::RCC::RCCResult run(bool /*timedout*/) {
    auto &props = this->m_properties;
    auto &port = this->in;
    // Shutdown if input port is finished
    if (props.stopOnEOF && port.eof()) {
      return OCPI::RCC::RCC_ADVANCE_DONE;
    }
    // Write message to file
    if (port.hasBuffer()) {
      if (props.messagesInFile) {
        header_t header = {
          .length = static_cast<uint32_t>(port.length()),
          .opcode = port.opCode()
        };
        this->_file_stream.write(reinterpret_cast<char*>(&header), HEADER_LENGTH);
        if (this->_file_stream.fail()) {
          return this->setError("can't write message header to file");
        }
      }
      if (port.length() > 0) {
        this->_file_stream.write(reinterpret_cast<char*>(port.data()), port.length());
        if (this->_file_stream.fail()) {
          return this->setError("can't write message to file");
        }
      }
      props.bytesWritten += port.length();
      props.messagesWritten += 1;
    }
    return OCPI::RCC::RCC_ADVANCE;
  }

  OCPI::RCC::RCCResult release() {
    if (this->_file_stream.is_open()) {
      try {
        this->_file_stream.close();
      } catch (std::exception &e) {
        return this->setError("Exception thrown when closing file");
      }
    }
    return OCPI::RCC::RCC_OK;
  }

};

FILE_WRITE_CC_START_INFO
FILE_WRITE_CC_END_INFO
