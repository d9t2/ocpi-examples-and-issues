// Copyright (c) 2023 Dominic Adam Walters
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

#include "timeout-worker.hh"

#include <chrono>

using namespace TimeoutWorkerTypes;

// RunCondition constants
static OCPI::RCC::PortMask NO_PORTS[1] = {OCPI::RCC::RCC_NO_PORTS};
typedef uint32_t timeout_in_usecs_t;
const static timeout_in_usecs_t DEFAULT_TIMEOUT = TIMEOUT_DEFAULT_TIMEOUT;
constexpr static bool ENABLE_TIMEOUT = true;

class TimeoutWorker : public TimeoutWorkerBase {

  OCPI::RCC::RunCondition _rc;

  std::chrono::time_point<std::chrono::steady_clock> time_of_last_run;

  public: TimeoutWorker()
    : _rc(NO_PORTS, DEFAULT_TIMEOUT, ENABLE_TIMEOUT),
      time_of_last_run(std::chrono::steady_clock::now()) {
  }

  OCPI::RCC::RCCResult start() {
    this->_rc.setTimeout(this->properties().timeout_us);
    this->setRunCondition(&this->_rc);
    return OCPI::RCC::RCC_OK;
  }

  OCPI::RCC::RCCResult run(bool timedout) {
    if (!timedout and this->firstRun()) {
      this->log(7, "`run` method ran for the first time; not a timeout");
    } else if (!timedout) {
      return this->setError("`run` method ran, but didn't timeout");
    } else {
      this->log(7, "`run` method ran after timeout");
    }
    const std::chrono::time_point<std::chrono::steady_clock> now = std::chrono::steady_clock::now();
    const auto duration = std::chrono::duration_cast<std::chrono::microseconds>(now - this->time_of_last_run);
    this->log(7,
      "Expected a timeout of %u microseconds, got %lu microseconds",
      this->properties().timeout_us,
      duration.count()
    );
    this->time_of_last_run = now;
    return OCPI::RCC::RCC_OK;
  }

};

TIMEOUT_START_INFO
TIMEOUT_END_INFO
